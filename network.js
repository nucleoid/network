const http = require("http");
const URL = require("url").URL;

module.exports.http = require("./http");
module.exports.https = require("./https");

module.exports.forward = function(req, res, string) {
  let url = new URL(string);

  let host = url.hostname;
  let port = url.port;
  let path = url.pathname;

  let request = http
    .request(
      {
        host,
        port,
        path,
        method: req.method,
        headers: req.headers
      },
      function(response) {
        let data = "";

        response.setEncoding("utf8");
        response.on("data", chunk => {
          data += chunk.toString();
        });

        response.on("end", function() {
          res.status(response.statusCode).send(data);
        });
      }
    )
    .on("error", function(e) {
      res.status(500).send({ message: e.message });
    });

  if (req.body !== undefined) {
    if (typeof req.body === "object") {
      request.write(JSON.stringify(req.body));
    } else {
      request.write(req.body);
    }
  }

  request.end();
};
