const { URL } = require("url");

module.exports = function(network, method, string, data, headers) {
  let url = new URL(string);

  let host = url.hostname;
  let port = url.port;
  let path = url.pathname;

  if (!headers) {
    headers = {};
  }

  if (typeof data === "object") {
    headers["content-type"] = "application/json";
  } else {
    headers["content-type"] = "text/plain";
  }

  return new Promise(function(resolve, reject) {
    let request = network
      .request(
        {
          host,
          port,
          path,
          method,
          headers
        },
        function(response) {
          let data = "";

          response.setEncoding("utf8");
          response.on("data", chunk => {
            data += chunk.toString();
          });

          response.on("end", function() {
            let contentType = response.headers["content-type"];

            if (contentType.startsWith("application/json")) {
              let json;

              try {
                json = !data || data === "" ? "" : JSON.parse(data);
              } catch (e) {
                reject(e);
              }

              resolve({
                status: response.statusCode,
                data: json,
                headers: response.headers
              });
            } else {
              resolve({
                status: response.statusCode,
                data,
                headers: response.headers
              });
            }
          });
        }
      )
      .on("error", function(e) {
        reject(e);
      });

    if (data !== undefined) {
      if (typeof data === "object") {
        request.write(JSON.stringify(data));
      } else {
        request.write(data);
      }
    }

    request.end();
  });
};
