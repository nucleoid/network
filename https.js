const https = require("https");
const call = require("./call");

module.exports.post = function(string, data, headers) {
  return call(https, "POST", string, data, headers);
};
