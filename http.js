const http = require("http");
const call = require("./call");

module.exports.post = function(string, data, headers) {
  return call(http, "POST", string, data, headers);
};
